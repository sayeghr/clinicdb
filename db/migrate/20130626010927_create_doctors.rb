class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name
      t.belongs_to :clinic
      t.integer :clinic_id
      t.timestamps
    end
  end
end
