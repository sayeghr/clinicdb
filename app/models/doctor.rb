class Doctor < ActiveRecord::Base
  belongs_to :clinic
  validates :clinic_id, presence: true
end
