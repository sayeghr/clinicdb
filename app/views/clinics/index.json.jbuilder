json.array!(@clinics) do |clinic|
  json.extract! clinic, : name
  json.url clinic_url(clinic, format: :json)
end
