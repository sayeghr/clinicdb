# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ClinicDB::Application.config.secret_key_base = 'e9a67c2c9ed6ef244efbea97e26f0d050b8045320a04a07a16b1e92abd1b3f136190da75277c45e94e633235d30501cb0966e825120e573f72c5a11a4c227a0e'
